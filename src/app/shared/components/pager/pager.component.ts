import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Pager, PagerService} from "../../services/pager/pager.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BasicService} from "../../services/basic.service";
import {CheckQueryParamsService, QueryParamsData} from "../../services/check-query-params/check-query-params.service";

export interface QueryForClickOnPage {
  page: number;
}

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.scss']
})
export class PagerComponent extends BasicService implements OnInit {

  @Input() allItems: any[] = [];
  @Input() currentPage?: number = 1;
  @Input() itemsPerPage?: number = 5;

  @Output() pagedItemsChanged = new EventEmitter<any[]>();

  pager = {} as Pager;

  pagedItems: any[] = [];

  queryCurrentPage: number;

  constructor(private pagerService: PagerService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private checkQueryParamsService: CheckQueryParamsService) {
    super();
  }

  ngOnInit() {
    this.checkQueryParams();
  }

  checkQueryParams() {
    this.subs.push(
      this.activatedRoute.queryParams.subscribe(
        () => {

          const queryParamsData = this.activatedRoute.snapshot.queryParams as QueryParamsData;

          this.queryCurrentPage = Number(queryParamsData.page);

          if (this.queryCurrentPage) {
            this.setPage(this.queryCurrentPage);
          }
        }
      )
    )
  }

  setPage(currentPage: number) {

    this.navigateWithNewQueryParams(currentPage);

    this.pager = this.pagerService.getPager(this.allItems.length, currentPage, this.itemsPerPage);

    this.router.navigate([],
      {
        queryParams: {'page': currentPage},
        queryParamsHandling: "merge"
      });

    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);

    this.pagedItemsChanged.emit(this.pagedItems);
  }


  navigateWithNewQueryParams(currentPage: number) {
    const queryParams: QueryForClickOnPage = {
      page: currentPage
    };

    this.checkQueryParamsService.navigateWithNewQueryParams(queryParams);

  }

}
