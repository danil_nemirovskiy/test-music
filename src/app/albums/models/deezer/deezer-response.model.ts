import {DeezerAlbumInfo} from "./deezer-album-info.model";

export class DeezerResponse {
  public data: DeezerAlbumInfo[];
  public next: string;
  public total: number;
}
