import {ItunesAlbumInfo} from "./itunes-album-info.model";

export class ItunesResponse {
  public resultsCount: number;
  public results: ItunesAlbumInfo[];
}
