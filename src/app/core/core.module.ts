import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {HttpClientJsonpModule, HttpClientModule} from "@angular/common/http";

import {DeezerStorageService} from "../shared/services/storage/deezer-storage.service";
import {ItunesStorageService} from "../shared/services/storage/itunes-storage.service";
import {BasicService} from "../shared/services/basic.service";
import {AppRoutingModule} from "../app-routing.module";
import {SearchService} from "../shared/services/search.service";
import {PagerService} from "../shared/services/pager/pager.service";
import {CheckQueryParamsService} from "../shared/services/check-query-params/check-query-params.service";


@NgModule({
  imports: [CommonModule, HttpClientModule, HttpClientJsonpModule, AppRoutingModule],
  providers: [
    SearchService,
    DeezerStorageService,
    ItunesStorageService,
    BasicService,
    PagerService,
    CheckQueryParamsService
  ]
})

export class CoreModule {
}
