import {Component, OnInit} from '@angular/core';

import {DeezerAlbumInfo} from "./models/deezer/deezer-album-info.model";
import {ItunesAlbumInfo} from "./models/itunes/itunes-album-info.model";

import {AlbumsService} from "./services/albums.service";
import {BasicService} from "../shared/services/basic.service";
import {SearchService} from "../shared/services/search.service";

import {forkJoin} from "rxjs";
import {finalize} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {
  CheckQueryParamsService,
  QueryParamsData
} from "../shared/services/check-query-params/check-query-params.service";


export interface QueryForSearchArtist {
  artist: string;
  page?: number;
}

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})


export class AlbumsComponent extends BasicService implements OnInit {

  normalizedAlbums = [];
  queryArtist: string;
  isPageNotFoundInQueryParams: boolean;
  isNeedToResetToFirstPage = false;

  constructor(private searchService: SearchService,
              private albumsService: AlbumsService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private checkQueryParamsService: CheckQueryParamsService
  ) {
    super();
  }


  ngOnInit() {
    this.checkQueryParams();
  }

  checkQueryParams() {
    this.subs.push(
      this.activatedRoute.queryParams.subscribe(
        () => {

          const queryParamsData = this.activatedRoute.snapshot.queryParams as QueryParamsData;

          this.queryArtist = queryParamsData.artist;

          this.isPageNotFoundInQueryParams = !queryParamsData.page;

          if (this.queryArtist) {
            this.getAllAlbums(this.queryArtist);
          }
        }
      )
    )
  }


  onChangeInputValue(event) {
    const searchArtist = event.target.value;

    this.isNeedToResetToFirstPage = true;


    this.getAllAlbums(searchArtist);
  }


  getAllAlbums(searchArtist: string) {

    this.navigateWithNewQueryParams(searchArtist);

    searchArtist = searchArtist.trim().toLowerCase();

    this.albumsService.setIsAlbumsLoading(true);

    this.subs.push(
      forkJoin(
        this.searchService.getDeezerAlbums(searchArtist),
        this.searchService.getItunesAlbums(searchArtist)
      )
        .pipe(
          finalize(
            () => {
              this.albumsService.setIsAlbumsLoading(false);
            }
          )
        )
        .subscribe(
          (result) => {

            const deezerAlbums: DeezerAlbumInfo[] = result[0];
            const itunesAlbums: ItunesAlbumInfo[] = result[1];

            this.normalizeAllAlbums(deezerAlbums, itunesAlbums);
          }
        )
    );
  }


  normalizeAllAlbums(deezerAlbums: DeezerAlbumInfo[], itunesAlbums: ItunesAlbumInfo[]) {
    this.normalizedAlbums = this.searchService.getUniqueAlbums(deezerAlbums, itunesAlbums);

    this.albumsService.setAlbums(this.normalizedAlbums);
  }


  navigateWithNewQueryParams(searchArtist: string) {

    const queryParams: QueryForSearchArtist = {
      artist: searchArtist,
    };

    if (this.isNeedToResetToFirstPage || this.isPageNotFoundInQueryParams) {
      queryParams.page = 1;
    }

    this.checkQueryParamsService.navigateWithNewQueryParams(queryParams);
  }

}
