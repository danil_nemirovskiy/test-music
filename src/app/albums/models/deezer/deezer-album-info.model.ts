import {DeezerArtist} from "./deezer-artist.model";

export class DeezerAlbumInfo {
  public artist: DeezerArtist;
  public id: number;
  public cover: string;
  public cover_big: string;
  public cover_medium: string;
  public cover_small: string;
  public cover_xl: string;
  public explicit_lyrics: boolean;
  public genre_id: number;
  public link: string;
  public nb_tracks: number;
  public record_type: string;
  public title: string;
  public tracklist: string;
  public type: string;
}
