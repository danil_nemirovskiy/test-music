import {CommonAlbum} from "../models/common-album.model";
import {Subject} from "rxjs";

export class AlbumsService {

  albumsChanged$ = new Subject<CommonAlbum[]>();
  isAlbumsLoadingChanged$ = new Subject<boolean>();

  setIsAlbumsLoading(isAlbumsLoading: boolean) {
    this.isAlbumsLoadingChanged$.next(isAlbumsLoading);
  }

  setAlbums(albums: CommonAlbum[]) {
    this.albumsChanged$.next(albums);
  }


}
