import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AlbumsComponent} from './albums/albums.component';
import {AlbumsListComponent} from './albums/albums-list/albums-list.component';
import {AlbumsListItemComponent} from './albums/albums-list/albums-list-item/albums-list-item.component';

import {AlbumsService} from "./albums/services/albums.service";
import {CoreModule} from "./core/core.module";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "./shared/shared.module";


@NgModule({
  declarations: [
    AppComponent,
    AlbumsComponent,
    AlbumsListComponent,
    AlbumsListItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CoreModule,
    SharedModule
  ],
  providers: [AlbumsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
