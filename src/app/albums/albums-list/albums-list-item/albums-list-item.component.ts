import {Component, Input, OnInit} from '@angular/core';
import {CommonAlbum} from "../../models/common-album.model";

@Component({
  selector: 'app-albums-list-item',
  templateUrl: './albums-list-item.component.html',
  styleUrls: ['./albums-list-item.component.scss']
})
export class AlbumsListItemComponent implements OnInit {

  @Input() album: CommonAlbum;

  constructor() { }

  ngOnInit() {}

}
