import {OnDestroy} from "@angular/core";
import {Subscription} from "rxjs";

export class BasicService implements OnDestroy {

  protected subs: Subscription[] = [];

  ngOnDestroy(): void {
    this.subs.forEach((sub: Subscription) => sub && sub.unsubscribe())
  }

}
