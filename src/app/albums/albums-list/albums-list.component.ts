import {Component, OnInit} from '@angular/core';
import {AlbumsService} from "../services/albums.service";
import {CommonAlbum} from "../models/common-album.model";
import {BasicService} from "../../shared/services/basic.service";


@Component({
  selector: 'app-albums-list',
  templateUrl: './albums-list.component.html',
  styleUrls: ['./albums-list.component.scss']
})
export class AlbumsListComponent extends BasicService implements OnInit {

  albums: CommonAlbum[] = [];
  isAlbumsEmpty = true;
  isAlbumsLoading: boolean;

  pagedAlbums: CommonAlbum[] = [];

  albumsEmptyTitle = 'Enter your artist for find albums!';

  constructor(private albumsService: AlbumsService) {
    super();
  }


  ngOnInit() {
    this.getAlbums();
  }

  getAlbums() {
    this.subs.push(
      this.albumsService.isAlbumsLoadingChanged$.subscribe(
        (isAlbumsLoading) => {
          this.isAlbumsLoading = isAlbumsLoading;
          if (this.isAlbumsLoading) {
            this.isAlbumsEmpty = false;
          }
        }
      ),

      this.albumsService.albumsChanged$
        .subscribe(
          (albums) => {
            this.albums = albums;
            this.isAlbumsEmpty = this.albums.length === 0;
            this.albumsEmptyTitle = 'Cannot find albums for your search request!'
          }
        )
    );
  }

  onPagedItemsChanged(pagedItems: CommonAlbum[]) {

    // way out with setTimeout

    setTimeout(() => {
      this.pagedAlbums = pagedItems;
    })
  }

}
