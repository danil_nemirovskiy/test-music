import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {map} from "rxjs/operators";
import {DeezerResponse} from "../../../albums/models/deezer/deezer-response.model";
import {environment} from "../../../../environments/environment";


@Injectable()

export class DeezerStorageService {

  constructor(private http: HttpClient) {
  }

  getDeezerAlbums(searchArtist: string) {

    const basicUrl = environment.api.deezerUrl + environment.api.routes.search + environment.api.additionalItem.album;

    const url = this.getUrlWithParams(basicUrl, searchArtist);

    return this.http.jsonp(url, "callback").pipe(
      map(
        (deezerResponse: DeezerResponse) => {
          return deezerResponse.data;
        }
      )
    );
  }

  getUrlWithParams(url: string, searchArtist: string) {
    let params = new HttpParams()
      .append('q', searchArtist)
      .append('output', 'jsonp');

    return url + '?' + params;
  }

}
