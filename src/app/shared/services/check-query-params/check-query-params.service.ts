import {BasicService} from "../basic.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Injectable} from "@angular/core";


export interface QueryParamsData {
  artist: string;
  page: string;
}

@Injectable()

export class CheckQueryParamsService extends BasicService {

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    super();
  }

  navigateWithNewQueryParams(queryParams) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: queryParams,
        queryParamsHandling: "merge"
      }
    )
  }


}
