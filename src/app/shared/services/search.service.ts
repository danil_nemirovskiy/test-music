import {ItunesStorageService} from "./storage/itunes-storage.service";
import {DeezerStorageService} from "./storage/deezer-storage.service";
import {Injectable} from "@angular/core";
import {CommonAlbum} from "../../albums/models/common-album.model";
import {DeezerAlbumInfo} from "../../albums/models/deezer/deezer-album-info.model";
import {ItunesAlbumInfo} from "../../albums/models/itunes/itunes-album-info.model";

@Injectable()

export class SearchService {

  protected uniqueAlbums: CommonAlbum[] = [];

  constructor(private itunesStorageService: ItunesStorageService, private deezerStorageService: DeezerStorageService) {
  }

  getItunesAlbums(searchArtist: string) {
    return this.itunesStorageService.getItunesAlbums(searchArtist);
  }

  getDeezerAlbums(searchArtist: string) {
    return this.deezerStorageService.getDeezerAlbums(searchArtist);
  }

  getUniqueAlbums(deezerAlbums: DeezerAlbumInfo[], itunesAlbums: ItunesAlbumInfo[]) {

    let album: CommonAlbum;
    let uniqueAlbums: CommonAlbum[] = [];
    const notNormalizedAlbums = [];

    deezerAlbums.forEach((currentAlbum: DeezerAlbumInfo) => {
      album = {
        title: currentAlbum.title,
        imageUrl: currentAlbum.cover,
        artistName: currentAlbum.artist.name
      };

      notNormalizedAlbums.push(album);
    });


    itunesAlbums.forEach((currentAlbum: ItunesAlbumInfo) => {

      album = {
        title: currentAlbum.collectionName,
        imageUrl: currentAlbum.artworkUrl100,
        artistName: currentAlbum.artistName
      };

      notNormalizedAlbums.push(album);
    });

    notNormalizedAlbums.map(
      x => uniqueAlbums.filter(
        a => a.title.trim().toLowerCase() == x.title.trim().toLowerCase()
      ).length > 0 ? null : uniqueAlbums.push(x));

    this.uniqueAlbums = uniqueAlbums;

    return this.uniqueAlbums;
  }

}
