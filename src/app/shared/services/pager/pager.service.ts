import {Injectable} from "@angular/core";


export interface Pager {
  totalPages: number;
  totalItems: number;
  currentPage: number;
  itemsPerPage: number;
  startPage: number;
  endPage: number;
  startIndex: number;
  endIndex: number;
  pages: number[];
}


@Injectable()

export class PagerService {

  getPager(totalItems: number, currentPage: number = 1, itemsPerPage: number = 5): Pager {

    // calculate total pages

    const totalPages = Math.ceil(totalItems / itemsPerPage);

    // ensure current page isn't out of range

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > totalPages) {
      currentPage = totalPages;
    }


    let startPage: number, endPage: number;

    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }


    // calculate start and end item indexes

    let startIndex = (currentPage - 1) * itemsPerPage;
    let endIndex = Math.min(startIndex + itemsPerPage - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control

    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    // return object with all pager properties required by the view

    const pager: Pager = {
      totalPages,
      totalItems,
      currentPage,
      itemsPerPage,
      startPage,
      endPage,
      startIndex,
      endIndex,
      pages
    };

    return pager;

  }

}
