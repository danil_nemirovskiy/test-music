export class CommonAlbum {
  public title: string;
  public imageUrl: string;
  public artistName: string;
}
