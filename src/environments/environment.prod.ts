export const environment = {
  production: true,
  api: {
    deezerUrl: 'https://api.deezer.com/',
    itunesUrl: 'https://itunes.apple.com/',
    routes: {
      search: 'search/'
    },
    additionalItem: {
      album: 'album/'
    }
  }
};
