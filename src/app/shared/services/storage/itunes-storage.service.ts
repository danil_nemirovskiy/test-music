import {HttpClient, HttpParams} from "@angular/common/http";
import {Injectable} from "@angular/core";

import {map} from "rxjs/operators";
import {ItunesResponse} from "../../../albums/models/itunes/itunes-response.model";
import {environment} from "../../../../environments/environment";


@Injectable()

export class ItunesStorageService {

  constructor(private http: HttpClient) {
  }

  getItunesAlbums(searchArtist: string) {

    const url = environment.api.itunesUrl + environment.api.routes.search;

    const params: HttpParams = new HttpParams()
      .append('entity', 'album')
      .append('term', searchArtist);


    return this.http.get(url, {params: params}).pipe(
      map(
        (itunesResponse: ItunesResponse) => {
          return itunesResponse.results;
        }
      )
    );
  }
}
